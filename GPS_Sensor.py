# Simple GPS module demon
# Will wait for a fix and print a message every
# second with the current location and other details.
import time
import board
import busio
import adafruit_gps
import serial  # Use the pyserial library for uart access
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-5s) %(message)s', )


def main_gps():
    uart = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=10)

    # Create a GPS module instance.
    gps = adafruit_gps.GPS(uart, debug=False)  # Use UART/pyserial

    # Turn on the basic GGA and RMC info
    gps.send_command(b"PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
    # Turn on just minimum info (RMC only, location):
    # gps.send_command(b'PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')
    # Turn off everything:
    # gps.send_command(b'PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')

    # Update once a second
    gps.send_command(b"PMTK220,1000")
    # Update once every two seconds
    # gps.send_command(b'PMTK220,2000')
    # Update twice a second
    # gps.send_command(b'PMTK220,500')

    # Main loop runs forever printing the location, etc. every second
    last_print = time.monotonic()
    while True:
        # Make sure to call gps.update() every loop iteration and at least twice
        # as fast as data comes from the GPS unit (usually every second).
        # This returns a bool that's true if it parsed new data (you can ignore it
        # though if you don't care and instead look at the has_fix property).
        gps.update()
        # Every second print out current location details if there's a fix.
        current = time.monotonic()
        if current - last_print >= 1.0:
            last_print = current
            if not gps.has_fix:
                # Try again if we don't have a fix yet.
                logging.debug("Waiting for Acquisition...")
                continue
            # We have a fix! (gps.has_fix is true)
            # Print out details about the fix like location, date, etc.
            # logging.debug("=" * 40)  # Print a separator line.
            logging.debug(
                "Fix timestamp: {}/{}/{} {:02}:{:02}:{:02}".format(
                    gps.timestamp_utc.tm_mon,  # Grab parts of the time from the
                    gps.timestamp_utc.tm_mday,  # struct_time object that holds
                    gps.timestamp_utc.tm_year,  # the fix time.  Note you might
                    gps.timestamp_utc.tm_hour,  # not get all data like year, day,
                    gps.timestamp_utc.tm_min,  # month!
                    gps.timestamp_utc.tm_sec,
                )
            )
            logging.debug("Lat : {0:.6f} deg".format(gps.latitude))
            logging.debug("Long: {0:.6f} deg".format(gps.longitude))
            logging.debug("Lock: {}".format(gps.fix_quality))

            # Uncomment this section for additional information
            # if gps.satellites is not None:
            #    logging.debug("# satellites: {}".format(gps.satellites))
            # if gps.altitude_m is not None:
            #    logging.debug("Altitude: {} meters".format(gps.altitude_m))
            # if gps.speed_knots is not None:
            #    logging.debug("Speed: {} knots".format(gps.speed_knots))
            # if gps.track_angle_deg is not None:
            #    logging.debug("Track angle: {} degrees".format(gps.track_angle_deg))
            # if gps.horizontal_dilution is not None:
            #    logging.debug("Horizontal dilution: {}".format(gps.horizontal_dilution))
            # if gps.height_geoid is not None:
            #    logging.debug("Height geo ID: {} meters".format(gps.height_geoid))
